import 'package:flutter/material.dart';
import 'package:project_a/user_entry_page.dart';

class UserListPage extends StatefulWidget {
  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  List<String> userList = [];
  bool isGridView = true;

  @override
  void initState() {
    super.initState();
    setListTextWidget();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        title: Text(
          'User List',
        ),
        actions: [
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return UserEntryPage();
                  },
                ),
              );
            },
            child: Icon(
              Icons.add,
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Row(
            children: [
              InkWell(
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      Icons.list,
                    ),
                  ),
                ),
                onTap: () {
                  setState(() {
                    isGridView = false;
                  });
                },
              ),
              SizedBox(width: 20),
              InkWell(
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      Icons.grid_3x3,
                    ),
                  ),
                ),
                onTap: () {
                  setState(() {
                    isGridView = true;
                  });
                },
              )
            ],
            mainAxisAlignment: MainAxisAlignment.end,
          ),
          Expanded(
            child: GridView.builder(
              itemBuilder: (context, index) {
                return Card(
                  elevation: 10,
                  shadowColor: Colors.red,
                  child: Text(
                    userList[index],
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                    ),
                  ),
                );
              },
              itemCount: userList.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: isGridView ? 2 : 1,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void setListTextWidget() {
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
    userList.add('List 1');
  }
}
