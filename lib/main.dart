import 'package:flutter/material.dart';
import 'package:project_a/pre_login_page.dart';
import 'package:project_a/screen_division_page.dart';
import 'package:project_a/splash_screen.dart';
import 'package:project_a/user_entry_page.dart';
import 'package:project_a/user_list_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.deepPurple,
        ),
        useMaterial3: true,
      ),
      home: UserEntryPage(),
    );
  }
}