import 'package:flutter/material.dart';

class SplashScreenPage extends StatefulWidget {
  @override
  State<SplashScreenPage> createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  // var appbarTitle = 'Splash Screen';
  Axis axisDirection = Axis.vertical;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: const Text(
          'Splash Screen',
          style: TextStyle(
            color: Colors.white,
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(20),
              ),
              padding: EdgeInsets.all(
                20,
              ),
              child: Wrap(
                direction: axisDirection == Axis.vertical
                    ? Axis.vertical
                    : Axis.horizontal,
                children: [
                  Icon(
                    Icons.ac_unit,
                    size: 20,
                    color: Colors.grey.shade300,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    'No Data Available',
                    style: TextStyle(
                      color: Colors.grey.shade300,
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
            ),
            TextButton(
              onPressed: () {
                setState(() {
                  axisDirection = (axisDirection == Axis.horizontal)
                      ? Axis.vertical
                      : Axis.horizontal;
                });
              },
              child: Text(
                'Click to Rotate Orientation',
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
