import 'package:flutter/material.dart';

class ScreenDivisonPage extends StatelessWidget {
  const ScreenDivisonPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          Expanded(
            child: Column(
              children: [
                getCustomExpandedWidget(Colors.red),
                getCustomExpandedWidget(Colors.blue),
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    color: Colors.white,
                  ),
                ),
                Expanded(
                    child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            children: [
                              getCustomExpandedWidget(
                                Colors.green,
                              ),
                              getCustomExpandedWidget(
                                Colors.orange,
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                            child: Column(
                              children: [
                                getCustomExpandedWidget(
                                  Colors.blue,
                                ),
                                getCustomExpandedWidget(
                                  Colors.black,
                                ),
                              ],
                            )),
                      ],
                    )),
                Expanded(
                    child: Container(
                      color: Colors.white,
                    )),
              ],
            ),
          ),
          getCustomExpandedWidget(Colors.amber),
        ],
      ),
    );
  }

  Widget getCustomExpandedWidget(containerColor, {containerFlex = 1}) {
    return Expanded(
      child: Container(
        color: containerColor,
      ),
      flex: containerFlex,
    );
  }
}
